# pyrunoff

Random-Forest regression to model the rainfall-runoff process.
The data used was recorded by arduino dataloggers powered by solar panels and meassuring rainfall and the water level at the outlet of the basin. 

## Raw data

- Data used:  Accumulated rainfall every 15 minutes, water level at the outlet of the basin.

- The recorded data can be accessed using the web: https://monitorreservasfe.streamlit.app/   

## Upstream data
- `rainfall-runoff-upstream.ipynb`

Prepare, clean and improve data.

- Lectura de datasets
- Concatenar todas las tormentas y escorrentías
- Filtrar datos, cada 15 minutos
- Armar columnas de datos descriptores: mm caídos, mm acumulados en tiempos t-i
- Visualizar

## Random Forest Regression

Features:

- 4 columnas con los mm15min desfasados en 1, 2, 3 y 4 lugares
- 4 columnas con el runoff (nivel) 1, 2, 3 y 4 lugares desfasados
- 1 columna con la precipitación acumulada (mm15min) en las 4 lecturas previas



