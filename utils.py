import plotly.graph_objects as go

def plot(x=None,y=None, label=None):
    fig = go.Figure()
    fig.add_trace(go.Scattergl(x=x, y=y, mode="markers+lines", name=label))
    return fig

def add_features(in_dataset):
    """tiene que tener una columna llamada mm15min"""
    out_dataset = in_dataset.copy()
    
    # columnas de mm15min desfasados
    out_dataset["mm15min-1"] = out_dataset["mm15min"].shift(1)
    out_dataset["mm15min-2"] = out_dataset["mm15min"].shift(2)
    out_dataset["mm15min-3"] = out_dataset["mm15min"].shift(3)
    out_dataset["mm15min-4"] = out_dataset["mm15min"].shift(4)
    
    # columna de mm15min acumulados de 3 lecturas previas
    out_dataset['accumulated_rainfall'] = out_dataset['mm15min'].rolling(window=3, min_periods=1).sum()
    
    # elimina las 4 primeras filas
    out_dataset.drop(0, inplace = True)
    out_dataset.drop(1, inplace = True)
    out_dataset.drop(2, inplace = True)
    out_dataset.drop(3, inplace = True)

    return out_dataset